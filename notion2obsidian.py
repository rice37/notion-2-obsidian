"""
Creates a command line to deal with notion files exported to markdown
"""
import sys
import argparse
import os
from mapper.notion_2_obsidian import markdown_notion_2_obsidian


def metadata(metadata_arguments: dict):
    """
    Reads the metadata from the file and prints it to stdout
    """
    filename = metadata_arguments.filename
    if not os.path.isfile(filename):
        print('File not found')
        sys.exit(1)
    print('Notion metadata for file: ' + filename)
    with open(filename, 'r', encoding='utf-8') as input_file:
        markdown_text = markdown_notion_2_obsidian(input_file.read())
        with open(filename, 'w', encoding='utf-8') as output_file:
            output_file.write(markdown_text)


def add_metadata_args(parser: argparse.ArgumentParser):
    """
    Adds the arguments for the metadata command
    """
    parser.add_argument(
        '--filename', '-f',
        required=True,
        type=str,
        help='The file to read',
    )


commands = {
    'metadata': {
        'action': metadata,
        'help': 'Add metadata to a file',
        'add_args': add_metadata_args,
        'parser': None,
    },
}
available_commands = commands.keys()

parser = argparse.ArgumentParser()
command_subparser = parser.add_subparsers(
    dest='command', help='sub-command help')
command_parsers = {}
for command in available_commands:
    commands[command]['parser'] = command_subparser.add_parser(
        command, help=commands[command]['help'])
    commands[command]['add_args'](commands[command]['parser'])

args = parser.parse_args()
commands[args.command].get('action')(args)
