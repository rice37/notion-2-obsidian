#!/bin/bash

file_to_check="$1"

echo $file_to_check

# if file_to_check is empty
if [ -z "$file_to_check" ]
then
    python -m unittest tests/**/test_*.py
    exit 0
fi

python -m unittest $file_to_check