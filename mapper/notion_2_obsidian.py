import json
import re
from notion.metadata import get_metadata_block
from notion.metadata import metadata_block_to_yaml
from notion.metadata import normalize_date
import urllib.parse


def get_config_from_file(config_file):
    with open(config_file) as f:
        config = json.load(f)
        return config


def replacement_matadata_keys(markdown_text, mapper_config):
    copy_text = markdown_text
    for key, value in mapper_config.items():
        copy_text = copy_text.replace(key, value)
    return copy_text


def format_dates(metadata_yml, dates_config='.config/obsidian-date-props.json'):
    dates = get_config_from_file(dates_config)
    unformat_regex = "^({}): .+$"
    regex = unformat_regex.format("|".join(dates))
    matches = re.findall(regex, metadata_yml, re.MULTILINE)
    result_metadata_yml = metadata_yml
    for match in matches:
        date_property_line = re.search(unformat_regex.format(
            match), metadata_yml, re.MULTILINE).group(0)
        date_string = date_property_line.split(': ')[1]
        date_normalized = normalize_date(date_string)
        result_metadata_yml = result_metadata_yml.replace(
            date_property_line, '{}: "{}"'.format(match, date_normalized))
    return result_metadata_yml


def markdown_notion_2_obsidian(markdown_text, json_config='.config/notion2obsidian.json'):
    mapper_config = get_config_from_file(json_config)
    result_markdown = markdown_text
    metadata_block = get_metadata_block(markdown_text)
    if metadata_block:
        result_markdown = result_markdown.replace(
            '{}'.format(metadata_block), '')
        metadata_yml = metadata_block_to_yaml(markdown_text)
        metadata_yml = replacement_matadata_keys(metadata_yml, mapper_config)
        metadata_yml = format_dates(metadata_yml)
        result_markdown = '{}{}'.format(metadata_yml, result_markdown)
    return result_markdown


def decode_uri(uri):
    return urllib.parse.unquote(uri)


def create_obsidian_link(string):
    return '[[{}]]'.format(string)


def transform_notion_string_link(string):
    regex_uid_32_digits = r'[a-f0-9]{32}'

    obsidian_link = string

    obsidian_link = decode_uri(obsidian_link)
    obsidian_link = re.sub(regex_uid_32_digits, '', obsidian_link)
    obsidian_link = obsidian_link.replace('.md', '')
    obsidian_links = obsidian_link.split(',')
    obsidian_links = [link.split('/')[-1] for link in obsidian_links]
    obsidian_links = [link.strip() for link in obsidian_links]
    obsidian_links = [create_obsidian_link(link) for link in obsidian_links]

    return ' - '.join(obsidian_links)


def transform_notion_mention_link(string):
    obsidian_link = re.sub(r'\(.+\)', '', string)
    obsidian_link = '[{}]'.format(obsidian_link)

    return obsidian_link


def map_link(link_url_notion):
    regex_notion_mention_link = r'\[.+\]\(.+\)'
    found_notion_metion_link = re.search(
        regex_notion_mention_link, link_url_notion)
    if not found_notion_metion_link:
        return transform_notion_string_link(link_url_notion)
    return transform_notion_mention_link(link_url_notion)
