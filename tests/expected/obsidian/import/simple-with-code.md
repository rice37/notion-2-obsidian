---
created: "2021-08-11 23:27:00"
done: Yes
due_date: "2021-08-13 00:00:00"
priority: Medium
tags: python, script, sh
updated: "2021-08-13 01:27:00"
---

# simple-with-code

```bash
*/5 * * * * python3 update_crobtabs.py
```

```bash
crontab -e << cat .config/crontabs/* .config/crontab_setup
```
