import re
from dateutil import parser


def get_tags_line(markdown_text):
    """
    Get tags line from markdown text.
    """
    regex = r'Tags: .+'
    return re.search(regex, markdown_text).group(0)


def get_tags(markdown_text):
    """
    Get tags from markdown text.
    """
    tags_line = get_tags_line(markdown_text)
    tags_line = tags_line.replace('Tags: ', '')
    tags = tags_line.split(',')
    return [tag.strip() for tag in tags]


def is_a_property(line):
    """
    Check if line is a property.
    """
    regex = r'.+: .+\w'


def get_metadata_block(markdown_text):
    """
    Get metadata block from markdown text.
    """
    regex = r'\n(.+: .+\n)+'
    result = re.search(regex, markdown_text)
    if result:
        return result.group(0)
    return None


def normalize_date(date_string):
    """
    Normalize date string.
    """
    date = parser.parse(date_string)
    return date.strftime("%Y-%m-%d %H:%M:%S")


def replace_last_break(markdown_text):
    """
    Replace last break from markdown text.
    """
    regex = r'\n$'
    return


def metadata_block_to_yaml(markdown_text):
    block = re.sub(r'(\n$|^\n)', '', get_metadata_block(markdown_text), 2)
    return '---\n{block}\n---\n\n'.format(block=block)
