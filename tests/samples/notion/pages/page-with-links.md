# Agregar el setup para las herramientas que se necesitan para comenzar a trabajar

Created at: September 14, 2021 2:32 PM
Done: Yes
Due Date: September 28, 2021
Priority: Low
Tags: thermo, work
Updated at: September 29, 2021 10:41 PM

- aws cli 2.0
  - python 3
- docker
  - minikube (recomendado)
  - docker desktop (se va hacer de pago)
  - docker-machine [docker/machine: Machine management for a container-centric world](../../Resources%20fecc94a7a3cb43b5a494163a88e61a49/docker%20machine%20Machine%20management%20for%20a%20container-%20ff7f81540be5456ea6e34634a48127fc.md)
-
