import unittest
from mapper.notion_2_obsidian import get_config_from_file
from mapper.notion_2_obsidian import replacement_matadata_keys
from mapper.notion_2_obsidian import markdown_notion_2_obsidian
from mapper.notion_2_obsidian import map_link
from parameterized import parameterized


class TestNotion2Obsidian(unittest.TestCase):

    def xtest_get_config_from_file(self):
        config = get_config_from_file('tests/mapper/.config.json')
        expected = {
            "hello1": "world",
            "hello2": [
                "world"
            ],
            "hello3": 1,
            "hello4": {
                "world": 1
            }
        }
        self.assertEqual(config, expected)

    @parameterized.expand([
        [
            '''
            Created at: August 11, 2021 11:27 PM
            Done: Yes
            Due Date: August 13, 2021
            Priority: High
            Tags: python, script, sh
            Updated at: August 13, 2021 1:27 AM
            ''',
            {
                "Created at": "created",
                "Done": "done",
                "Due Date": "due_date",
                "Priority": "priority",
                "Tags": "tags",
                "Updated at": "updated",
            },
            '''
            created: August 11, 2021 11:27 PM
            done: Yes
            due_date: August 13, 2021
            priority: High
            tags: python, script, sh
            updated: August 13, 2021 1:27 AM
            '''
        ],
        [
            '''
            Created at: April 11, 2021 11:27 PM
            Priority: Medium
            Tags: python, script, sh
            Updated at: October 13, 2021 1:27 AM
            Archived at: November 13, 2021 1:27 AM
            ''',
            {
                "Created at": "created",
                "Updated at": "updated",
                "Archived at": "archived",
            },
            '''
            created: April 11, 2021 11:27 PM
            Priority: Medium
            Tags: python, script, sh
            updated: October 13, 2021 1:27 AM
            archived: November 13, 2021 1:27 AM
            '''
        ],
    ])
    def xtest_replacement_metadata_keys(self, markdown_text, mapper_config, expected_markdown_text):
        actual_markdown_text = replacement_matadata_keys(
            markdown_text, mapper_config)
        self.assertEqual(actual_markdown_text, expected_markdown_text)

    @parameterized.expand([
        ['./tests/samples/notion/pages/simple-with-code.md',
            './tests/expected/obsidian/import/simple-with-code.md'],
        ['./tests/samples/notion/pages/only-props.md',
            './tests/expected/obsidian/import/only-props.md'],
        ['./tests/samples/notion/pages/links-and-code.md',
            './tests/expected/obsidian/import/links-and-code.md'],
        ['./tests/samples/notion/pages/weird-tags.md',
            './tests/expected/obsidian/import/weird-tags.md'],
    ])
    def test_markdown_notion_2_obsidian(self, markdown_filename, expected_filename):
        given_md = None
        expected_md = None
        with open(markdown_filename, 'r') as f:
            given_md = f.read()
        with open(expected_filename, 'r') as f:
            expected_md = f.read()
        actual_md = markdown_notion_2_obsidian(given_md)
        self.assertEqual(actual_md, expected_md)

    @parameterized.expand([
        [
            'Projects%20b3366964519749ad9f9e39d130b30bc9/Tocar%20en%20publico%206bd0fb6cf1fa46a6b081e457adbc2ba9.md',
            '[[Tocar en publico]]'
        ],
        [
            'Projects%20b3366964519749ad9f9e39d130b30bc9/Estilo%20de%20vestir%20propio%20b3ace971b7814c5ba09e056df51b2861.md',
            '[[Estilo de vestir propio]]'
        ],
        [
            'Knowledge%20b98367ce52664faa9066799e5b05dd64/kiwis%206a01d3d1a7934d2bbaec5b15c5504166.md, Knowledge%20b98367ce52664faa9066799e5b05dd64/Thermo%20Fisher%20organization%20concepts%200352224e746d4c8c8f96dae3924308d3.md',
            '[[kiwis]] - [[Thermo Fisher organization concepts]]'
        ],
        [
            '[docker/machine: Machine management for a container-centric world](../../Resources%20fecc94a7a3cb43b5a494163a88e61a49/docker%20machine%20Machine%20management%20for%20a%20container-%20ff7f81540be5456ea6e34634a48127fc.md)',
            '[[docker/machine: Machine management for a container-centric world]]'
        ]
    ])
    def test_map_link(self, link, expected_link_text):
        actual_link_text = map_link(link)
        self.assertEqual(actual_link_text, expected_link_text)
