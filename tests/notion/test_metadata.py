import unittest
from notion.metadata import get_tags as get_tags, get_metadata_block, normalize_date, metadata_block_to_yaml
from parameterized import parameterized


class TestMetadata(unittest.TestCase):
    @parameterized.expand([
        ['./tests/samples/notion/pages/simple-with-code.md',
            ['python', 'script', 'sh']],
        ['./tests/samples/notion/pages/only-props.md',
            ['armando', 'change management', 'curso induccion', 'greg', 'kiwis', 'permissions', 'thermo']],
        ['./tests/samples/notion/pages/links-and-code.md',
            ['arkus', 'hernan', 'luis', 'postgresql', 'programming', 'vault-core', 'work', 'wrethink']],
    ])
    def test_get_tags(self, filename, expected_tags):
        with open(filename, 'r') as markdown_text:
            tags = get_tags(markdown_text.read())
            self.assertEqual(tags, expected_tags)

    @parameterized.expand([
        ['./tests/samples/notion/pages/simple-with-code.md',
            './tests/expected/notion/metadata/simple-with-code.block.md'],
        ['./tests/samples/notion/pages/only-props.md',
            './tests/expected/notion/metadata/only-props.block.md'],
        ['./tests/samples/notion/pages/links-and-code.md',
            './tests/expected/notion/metadata/links-and-code.block.md'],
    ])
    def test_get_metadata_block(self, filename, expected_block):
        with open(filename, 'r') as markdown_text:
            block = get_metadata_block(markdown_text.read())
            with open(expected_block, 'r') as expected_block_file:
                self.assertEqual(block, expected_block_file.read())

    @parameterized.expand([
        ['August 11, 2021 11:27 PM', '2021-08-11 23:27:00'],
        ['August 13, 2021', '2021-08-13 00:00:00'],
        ['August 13, 2021 1:27 AM', '2021-08-13 01:27:00']
    ])
    def test_normalize_date(self, date, expected_date):
        self.assertEqual(normalize_date(date), expected_date)

    @parameterized.expand([
        ['./tests/samples/notion/pages/simple-with-code.md',
            './tests/expected/notion/metadata/simple-with-code.yml'],
        ['./tests/samples/notion/pages/only-props.md',
            './tests/expected/notion/metadata/only-props.yml'],
        ['./tests/samples/notion/pages/links-and-code.md',
            './tests/expected/notion/metadata/links-and-code.yml'],
    ])
    def test_metadata_block_to_yaml(self, filename, expected_yaml):
        with open(filename, 'r') as markdown_text:
            yaml = metadata_block_to_yaml(markdown_text.read())
            with open(expected_yaml, 'r') as expected_yaml_file:
                self.assertEqual(yaml, expected_yaml_file.read())
