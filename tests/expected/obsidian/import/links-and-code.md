---
created: "2021-08-17 11:36:00"
done: Yes
due_date: "2021-08-17 00:00:00"
priority: High
tags: arkus, hernan, luis, postgresql, programming, vault-core, work, wrethink
updated: "2021-08-17 18:27:00"
---

# links-and-codeVaultSnapshot

# Description

el comando de `getVaultSnapshot` esta fallando por el formato de la fecha de algunos assets en `details->>"createdAt"`

---

# Material

[https://wrethink.atlassian.net/browse/SD-5963](https://wrethink.atlassian.net/browse/SD-5963)

[https://github.com/wrethink/vault-core/pull/875](https://github.com/wrethink/vault-core/pull/875)

[https://github.com/wrethink/command-core/pull/877](https://github.com/wrethink/command-core/pull/877)

[https://github.com/wrethink/vault-core/blob/db00fc874706e85b91c46913e3f15627817126a2/db/DAL/asset.dal.js#L69](https://github.com/wrethink/vault-core/blob/db00fc874706e85b91c46913e3f15627817126a2/db/DAL/asset.dal.js#L69)

## PR fix

[https://github.com/wrethink/vault-core/pull/912](https://github.com/wrethink/vault-core/pull/912)

![Untitled](Revisar%20porque%20en%20QA%20no%20funciona%20el%20comando%20de%20get%20a73343e4922b4edbadaedc037628873d/Untitled.png)

## Queries

### Dev

```sql
select to_timestamp('2021-07-07T17:51:41.000Z', 'YYYY-MM-DD HH24:MI:SS.MSZ') -- pass
select to_timestamp('2021-07-07T17:51:41.000Z', 'YYYY-MM-DDTHH24:MI:SS.MSZ') -- fail
```

### QA

```sql
select to_timestamp('2021-07-07T17:51:41.000Z', 'YYYY-MM-DD HH24:MI:SS.MSZ') -- fail
select to_timestamp('2021-07-07T17:51:41.000Z', 'YYYY-MM-DDTHH24:MI:SS.MSZ') -- pass
```

### Replace analyze

**with**

![Untitled](Revisar%20porque%20en%20QA%20no%20funciona%20el%20comando%20de%20get%20a73343e4922b4edbadaedc037628873d/Untitled%201.png)

**without**

![Untitled](Revisar%20porque%20en%20QA%20no%20funciona%20el%20comando%20de%20get%20a73343e4922b4edbadaedc037628873d/Untitled%202.png)

**Other queries**

```sql
-- DEV
explain ANALYZE
SELECT date_trunc('day',
	TO_TIMESTAMP(
       A.details->>'createdAt',
       'YYYY-MM-DD HH24:MI:SS.MS'
    )
AT TIME ZONE INTERVAL '-07:00') as "snapshotDates", COUNT(*)
FROM
  public."Asset" as A
  LEFT JOIN public."Bundle" as B on B.id = A.bundle
WHERE
  B.owner = '084d7e18-ddea-4339-9e52-4a7f0759a0a1'
GROUP BY "snapshotDates"
ORDER BY "snapshotDates" desc
```

```sql
-- QA
SELECT date_trunc('day',
	TO_TIMESTAMP(
       replace(A.details->>'createdAt', 'T', ' '),
       'YYYY-MM-DD HH24:MI:SS.MS'
    )
AT TIME ZONE INTERVAL '-07:00') as "snapshotDates", COUNT(*)
FROM
  public."Asset" as A
  LEFT JOIN public."Bundle" as B on B.id = A.bundle
WHERE
  B.owner = '1efccd33-d2f1-4534-813e-6ea7e7a8ab81'
GROUP BY "snapshotDates"
ORDER BY "snapshotDates" desc
```

# References

[https://www.postgresql.org/docs/10/functions-formatting.html](https://www.postgresql.org/docs/10/functions-formatting.html)

[https://www.postgresql.org/docs/12/functions-formatting.html](https://www.postgresql.org/docs/12/functions-formatting.html)
