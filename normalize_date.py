import sys
from notion.metadata import normalize_date

to_parse_dates = sys.argv[1:]


def is_string(s):
    return isinstance(s, str)


if len(to_parse_dates) == 1 and is_string(to_parse_dates[0]):
    to_parse_dates = to_parse_dates[0].split(',')
    to_parse_dates = [x.strip() for x in to_parse_dates]

for date in to_parse_dates:
    try:
        print(normalize_date(date))
    except:
        print("Invalid input date:", date)
