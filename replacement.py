import json


def get_replacement_keys(configuration_path='./replacements.json'):
    replacement_key_values = {}
    with open(configuration_path) as f:
        replacement_key_values = json.load(f)


def replace_inline(line, term, replacement):
    if term in line:
        return line.replace(term, replacement)
    return line


def find_and_replace(lines, term, replacement):
    for index, line in enumerate(lines):
        lines[index] = replace_inline(line, term, replacement)
    return lines
