import sys
import re
import os
from replacement import find_and_replace, get_replacement_keys

filename = sys.argv[1]
print("-------- cleaning: ", filename)


def get_start_end_props_section(lines):
    prev_line_contains_props = False
    start_index = -1
    end_index = -1
    for index, line in enumerate(lines):
        has_prop = re.match(".+: ", line)
        if has_prop and start_index == -1:
            start_index = index
            prev_line_contains_props = True
        elif not has_prop and prev_line_contains_props:
            end_index = index
            break
    return start_index, end_index


def move_props_to_metadata_section(lines):
    start, end = get_start_end_props_section(lines)
    is_props_section_defined = start > -1 and end > -1
    if not is_props_section_defined:
        return lines
    return ['---\n'] + lines[start:end] + ['---\n'] + lines[:start] + lines[end:]


def normalize_tag(tag):
    return tag.lower().strip().replace(' ', '-')


def normalize_tags(lines):
    separator = ','
    for index, line in enumerate(lines):
        if 'tags: ' in line:
            tags_str = line.split(': ')[1]
            tags_line = 'tags: ' + \
                separator.join(map(normalize_tag, tags_str.split(separator)))
            lines[index] = re.sub(r"tags: .+", tags_line, line)
    return lines


new_lines = []

with open(filename) as f:
    lines = f.readlines()
    replacement_key_values = get_replacement_keys()
    for key, value in replacement_key_values.items():
        lines = find_and_replace(lines, value, key)
    lines = normalize_tags(lines)
    lines = move_props_to_metadata_section(lines)
    new_lines = lines

new_filename = re.sub(r" [A-Za-z0-9]{32}", "", filename)

new_directory_path = os.path.dirname(new_filename)

if not os.path.exists(os.path.dirname(new_filename)):
    try:
        os.makedirs(os.path.dirname(new_filename))
    except OSError as exc:  # Guard against race condition
        if exc.errno != errno.EEXIST:
            raise

print("...trying to open:", new_filename)
with open(new_filename, "w") as f:
    for line in new_lines:
        f.write(line)

print("new file created:", new_filename)

# os.remove(filename)
