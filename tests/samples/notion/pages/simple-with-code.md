# simple-with-code

Created at: August 11, 2021 11:27 PM
Done: Yes
Due Date: August 13, 2021
Priority: Medium
Tags: python, script, sh
Updated at: August 13, 2021 1:27 AM

```bash
*/5 * * * * python3 update_crobtabs.py
```

```bash
crontab -e << cat .config/crontabs/* .config/crontab_setup
```
