## Notion 2 Obsidian

I just wanted a way to import from Notion 2 Obsidian.

## Setup
### Prerequisites
- pyenv
- pipenv

### Install project
```bash
git clone $THIS_REPO $REPO_DIRECTORY
cd $REPO_DIRECTORY
pipenv install
pipenv shell
chmod u+x .test.sh
./.test.sh
```

## Usage
```bash
# 1
find ../$NOTION_EXPORT_FOLDER -type f | grep -E '[0-9a-fA-F]{32}\.md' | xargs -L 1 -I F python notion2obsidian.py metadata "F"
# 2 linking
# 3 smart linking
```
